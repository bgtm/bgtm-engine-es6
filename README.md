# BGTM - Engine for compiling a ES6 code into JS.

This is an engine which can be used in conjunction with
[BGTM](https://www.npmjs.com/package/bgtm) for
the purposes of processing a "application" built in es6 into a bundled
single file.

# How to use

Read the how to section for [BGTM](https://www.npmjs.com/package/bgtm).

Run:

    npm install --save-dev bgtm-engine-es6

In your gulpfile add the following dependency:

    var js = require('bgtm-engine-es6');

Then add the following task.

    tm.add('app-name', {
        runOnBuild: true,
        watch: true,
        watchSource: [
            'src/js/app/**/*.es6'
        ],
        liveReload: true,
        engine: es6,
        engineOptions: {
            'src': 'src/js/app/main.es6',
            'dest': 'dest/js/',
            'fileName': 'main.min.js'
        }
    });

# What it does

* Gets the source files specified in the `src` property.
* Compiles the ES6 "application" into a single file using
  [gulp-bro](https://www.npmjs.com/package/gulp-bro).
* Gulp Bro also minifies the file using uglify
* Extracts the sourcemaps from the compiled js files and exports them
  into their own files.
* Outputs the result to the destination specified in the `dest` property.

# Engine Options

## Defaults

    {
        src: '',
        dest: '',
        fileName: 'app.js',
        babelifyOptions: {},
        presetEnvOptions: {}
    }

| Option           | Mandatory | Description |
|------------------|-----------|-------------|
| src              | yes       | The source path to look for changes, This could be a string or an array of paths which can be patterns as well as paths. |
| dest             | yes       | The destination file for the output to be placed into. |
| fileName         | yes       | The name of the file to be output to the destination folder. |
| bablifyOptions   | no        | Allows for overriding the babelify options for the engine, e.g. if you want to turn the global on or off or ignore certain paths from transpiling etc. 
| presetEnvOptions | no        | Allows for overriding the @babel/preset-env options. |

# License

Copyright 2017-2019 Ben Blanks

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.