var plumber = require('gulp-plumber');
var bro = require('gulp-bro');
var babelify = require('babelify');
var uglifyify = require('uglifyify');
var exorcist = require('exorcist');
var transform = require('vinyl-transform');
var rename = require('gulp-rename');
var deepExtend = require('deep-extend');
var preset = require('@babel/preset-env');
/**
 * This is the default engine for parsing es6 files into a js file that can be understood
 * by the most common browsers in use today/
 *
 * @param taskManager
 * @param args
 * @param {Function} done callback used to tell bgtm that the engine is finished processing.
 * @returns {*}
 */
module.exports = function (taskManager, args, done) {
    var defaults = {
        src: '',
        dest: '',
        fileName: 'app.js',
        babelifyOptions: {},
        presetEnvOptions: {}
    };
    var engineOptions = deepExtend({}, defaults, args);

    // Ensure that node_modules are compiled by default.
    var babelifyDefaults = {
        global: true
    };
    var babelifyOptions = deepExtend({}, babelifyDefaults, engineOptions.babelifyOptions);

    // Ensure that presets does not get overridden.
    babelifyOptions.presets = [
        [preset, engineOptions.presetEnvOptions]
    ];

    // Build up the transforms to be added to this engine.
    var transforms = [];
    transforms.push([babelify, babelifyOptions]);

    // If in production then we want to compile the js.
    if (taskManager.isEnvironment(taskManager.environment.PRODUCTION)) {
        transforms.push([uglifyify, {global: true}]);
    }

    return this.src(engineOptions.src)
        .pipe(plumber())
        .pipe(bro({
            debug: true,
            transform: transforms
        }))
        .pipe(transform(function () {
            return exorcist(engineOptions.dest+'/'+engineOptions.fileName+'.map')
        }))
        .pipe(rename(engineOptions.fileName))
        .pipe(this.dest(engineOptions.dest))
        .pipe(done());
};